import os, sys
#sys.path.insert(0, os.path.abspath("./../dktorecuperedb"))
sys.path.insert(0, os.path.abspath("./.."))
sys.path.insert(0, os.path.abspath("./../.."))

from dktotoolkit import LoadEnv
import dktoparserhtml
from dktorecuperedb.office import Office
from dktorecuperedb.compendium import Compendium
from dktorecuperedb.bible import Bible, BibleOneYear


def testOffice():
    #date = "2023-05-30"
    cal  = "france"
    office = "vepres"
    format_output="html"
    format_output="simple_html"
    format_output="markdown"

    #format_output="native" #TODO

    #o = Office(date=date, calendar=cal, office=office, format_output=format_output)
    o = Office(calendar=cal, office=office, format_output=format_output)
    o.check_input()
    datas = o.get_office()
    print(datas)
#endDef

def testCompendium():

    #comp = Compendium(collection=collection, title=name, disambiguation=dis, language=lang)
    comp = Compendium(collection="Compendium", title="Credo", disambiguation='Nicée-Constantinople', language='latin')
    datas = comp.get_index()
    print(datas)
    comp.name2data()
    datas = comp.to_dict()
    print(datas)
#

def testBible():
    content = Bible("Jn", "1:1-5")
    content.ref2text()
    print(content.text)
    # TODO : unifier les sorties + ajouter option au parser "html_aelf" et ajouter ici le parser.
    # TODO : Permettre l'utilisation de jn, jean, Jean
#endDef


def testBible1Y():
    day = 1
    refsBible = BibleOneYear()
    datas=refsBible.on_a_year(day)
    print(datas)

#endDEf

if __name__=="__main__":
    LoadEnv()
    print("============================\Office......:")
    testOffice()
    print("============================\nCompendium.:")
    #testCompendium()
    print("============================\nBible......:")
    #testBible()
    print("============================\nBible1year.:")
    #testBible1Y()
    print("TODO : refaire DB Bible + cles primaires etc")
#endIf
