# Attention : copié collé du terminal change le trait d'union qui devient ascii visiblemen (u002d) alors qu'il est u2010 dans le retour

CONTENT_OFFICE_MARKDOWN = {
  "informations": {
    "source": "aelf",
    "zone": "romain",
    "calendrier": "france",
    "date": "2022-06-01",
    "nom": "vepres",
    "couleur": "rouge",
    "annee": None,
    "temps_liturgique": "pascal",
    "semaine": "7ème Semaine du Temps Pascal",
    "jour": "mercredi",
    "jour_liturgique_nom": "de la férie",
    "fete": "S. Justin, martyr",
    "degre": None,
    "ligne1": "mercredi, 7ème Semaine du Temps Pascal",
    "ligne2": "S. Justin, martyr",
    "ligne3": "Mémoire",
    "couleur2": None,
    "couleur3": None
  },
  "vepres": [
    {
      "id_deroule": "0",
      "cle_element": "se\\_signer\\_office",
      "titre_particulier": None,
      "ajouter_doxologie": None,
      "element_defaut": "1",
      "titre": None,
      "texte": "*(Faire le signe de Croix, au nom du Père, du Fils et du Saint Esprit, pendant **Dieu, viens à mon aide**)*",
      "editeur": None,
      "auteur": None,
      "reference": None
    },
    {
      "id_deroule": "1",
      "cle_element": "introduction",
      "titre_particulier": "Introduction",
      "ajouter_doxologie": None,
      "element_defaut": "aelf",
      "titre": None,
      "texte": "**V/** Dieu, viens à mon aide,\n**R/** Seigneur, à notre secours.\n Gloire au Père, et au Fils et au Saint‐Esprit,\nau Dieu qui est, qui était et qui vient,\npour les siècles des siècles.\nAmen. (Alléluia.)\n",
      "editeur": None,
      "auteur": None,
      "reference": None
    },
    {
      "id_deroule": "2",
      "cle_element": "hymne",
      "titre_particulier": "Hymne",
      "ajouter_doxologie": None,
      "element_defaut": "aelf",
      "titre": "Esprit de Dieu, tu es le feu",
      "texte": "Esprit de Dieu, tu es le feu,\nPatiente braise dans la cendre,\nA tout moment prête à surprendre\nLe moindre souffle et à sauter\nComme un éclair vif et joyeux\nPour consumer en nous la paille,\nEprouver l’or aux grandes flammes\nDu brasier de ta charité.\n\nEsprit de Dieu, tu es le vent,\nOù prends‐tu souffle, à quel rivage?\nÉlie se cache le visage\nA ton silence frémissant\nAux temps nouveaux tu es donné,\nSoupir du monde en espérance,\nPartout présent comme une danse,\nEclosion de ta liberté.\n\nEsprit de Dieu, tu es rosée\nDe joie, de force et de tendresse,\nTu es la pluie de la promesse\nSur une terre abandonnée.\nJaillie du Fils ressuscité,\nTu nous animes, source claire,\nEt nous ramènes vers le Père,\nAu rocher de la vérité.\n",
      "editeur": "CNPL",
      "auteur": "CFC",
      "reference": None
    },
    {
      "id_deroule": "3",
      "cle_element": "antienne\\_1",
      "titre_particulier": "Antienne",
      "ajouter_doxologie": None,
      "element_defaut": "aelf",
      "titre": None,
      "texte": "Votre tristesse deviendra joie. Alléluia.\n",
      "editeur": None,
      "auteur": None,
      "reference": None
    },
    {
      "id_deroule": "4",
      "cle_element": "psaume\\_1",
      "titre_particulier": "Psaume",
      "ajouter_doxologie": "True",
      "element_defaut": "aelf",
      "titre": None,
      "texte": "**1 ** Quand le Seigneur ramena les capt__i__fs à Sion,**\\***\nnous éti__o__ns comme en rêve !\n**2 ** Alors notre bouche était pl__e__ine de rires,\nnous poussi__o__ns des cris de joie ; **+**\nalors on disait parm__i__ les nations :\n« Quelles merveilles fait pour e__u__x le Seigneur ! » **\\***\n**3 ** Quelles merveilles le Seigne__u__r fit pour nous :\nnous éti__o__ns en grande fête !\n\n**4 ** Ramène, Seigne__u__r, nos captifs,\ncomme les torr__e__nts au désert.\n\n**5 ** Qui s__è__me dans les larmes\nmoiss__o__nne dans la joie : **+**\n**6 ** il s’en va, il s’en v__a__ en pleurant,\nil j__e__tte la semence ; **\\***\nil s’en vient, il s’en vi__e__nt dans la joie,\nil rapp__o__rte les gerbes.\n",
      "editeur": None,
      "auteur": None,
      "reference": "125"
    },
    {
      "id_deroule": "5",
      "cle_element": "doxologie",
      "texte": "**V/**Gloire au Père, et au Fils, et au Saint‐Esprit\n**R/**Pour les siècles des siècles.\nAmen.\n",
      "titre": "Acte de contrition",
      "editeur": None,
      "auteur": None,
      "reference": "Compendium",
      "disambiguation": None,
      "langue": "francais"
    },
    {
      "id_deroule": "6",
      "cle_element": "antienne\\_1",
      "titre_particulier": "Antienne",
      "ajouter_doxologie": None,
      "element_defaut": "aelf",
      "titre": None,
      "texte": "Votre tristesse deviendra joie. Alléluia.\n",
      "editeur": None,
      "auteur": None,
      "reference": None
    },
    {
      "id_deroule": "7",
      "cle_element": "antienne\\_2",
      "titre_particulier": "Antienne",
      "ajouter_doxologie": None,
      "element_defaut": "aelf",
      "titre": None,
      "texte": "Que nous vivions, que nous mourions, c’est pour le Seigneur, alléluia.\n",
      "editeur": None,
      "auteur": None,
      "reference": None
    },
    {
      "id_deroule": "8",
      "cle_element": "psaume\\_2",
      "titre_particulier": "Psaume",
      "ajouter_doxologie": "True",
      "element_defaut": "aelf",
      "titre": None,
      "texte": "**1 ** Si le Seigneur ne bât__i__t la maison,\nles bâtisseurs trav__a__illent en vain ; **\\***\nsi le Seigneur ne g__a__rde la ville,\nc’est en vain que v__e__illent les gardes.\n\n**2 ** En vain tu dev__a__nces le jour,\ntu retardes le mom__e__nt de ton repos, **+**\ntu manges un p__a__in de douleur : **\\***\nDieu comble son bien‐aim__é__ quand il dort.\n\n**3 ** Des fils, voilà ce que d__o__nne le Seigneur,\ndes enfants, la récomp__e__nse qu’il accorde ; **\\***\n**4 ** comme des flèches aux m__a__ins d’un guerrier,\nainsi les f__i__ls de la jeunesse.\n\n**5 ** Heureux l’h__o__mme vaillant\nqui a garni son carqu__o__is de telles armes ! **\\***\nS’ils affrontent leurs ennem__i__s sur la place,\nils ne seront p__a__s humiliés.\n",
      "editeur": None,
      "auteur": None,
      "reference": "126"
    },
    {
      "id_deroule": "9",
      "cle_element": "doxologie",
      "texte": "**V/**Gloire au Père, et au Fils, et au Saint‐Esprit\n**R/**Pour les siècles des siècles.\nAmen.\n",
      "titre": "Acte de contrition",
      "editeur": None,
      "auteur": None,
      "reference": "Compendium",
      "disambiguation": None,
      "langue": "francais"
    },
    {
      "id_deroule": "10",
      "cle_element": "antienne\\_2",
      "titre_particulier": "Antienne",
      "ajouter_doxologie": None,
      "element_defaut": "aelf",
      "titre": None,
      "texte": "Que nous vivions, que nous mourions, c’est pour le Seigneur, alléluia.\n",
      "editeur": None,
      "auteur": None,
      "reference": None
    },
    {
      "id_deroule": "11",
      "cle_element": "antienne\\_3",
      "titre_particulier": "Antienne",
      "ajouter_doxologie": None,
      "element_defaut": "aelf",
      "titre": None,
      "texte": "Tout vient de lui, tout est pour lui, tout est en lui ! Gloire à Dieu dans les siècles !\n",
      "editeur": None,
      "auteur": None,
      "reference": None
    },
    {
      "id_deroule": "12",
      "cle_element": "psaume\\_3",
      "titre_particulier": "Psaume",
      "ajouter_doxologie": "True",
      "element_defaut": "aelf",
      "titre": None,
      "texte": "**12 ** Rendons grâce à Die__u__ le Père, **+**\nlui qui nous a donné\nd’avoir part à l’hérit__a__ge des saints, **\\***\nd__a__ns la lumière.\n\n**13 ** Nous arrachant à la puiss__a__nce des ténèbres, **+**\nil nous a placés\ndans le Royaume de son F__i__ls bien‐aimé : **\\***\n**14 ** en lui nous avons le rachat,\nle pard__o__n des péchés.\n\n**15 ** Il est l’image du Die__u__ invisible, **+**\nle premier‐né, avant to__u__te créature : **\\***\n**16 ** en lui, tout fut créé,\ndans le ci__e__l et sur la terre.\n\nLes êtres vis__i__bles et invisibles, **+**\npuissances, principautés,\nsouverainet__é__s, dominations, **\\***\ntout est créé par lu__i__ et pour lui.\n\n**17 ** Il est avant to__u__te chose,\net tout subs__i__ste en lui.\n\n**18 ** Il est aussi la t__ê__te du corps, la tête de l’Église : **+**\nc’est lui le commencement,\nle premier‐n__é__ d’entre les morts, **\\***\nafin qu’il ait en to__u__t la primauté.\n\n**19 ** Car Dieu a jugé bon\nqu’habite en lui to__u__te plénitude **\\***\n**20 ** et que tout, par le Christ,\nlui soit enf__i__n réconcilié,\n\nfaisant la paix par le s__a__ng de sa Croix, **\\***\nla paix pour tous les êtres\nsur la t__e__rre et dans le ciel.\n",
      "editeur": None,
      "auteur": None,
      "reference": "CANTIQUE (Col 1)"
    },
    {
      "id_deroule": "13",
      "cle_element": "doxologie",
      "texte": "**V/**Gloire au Père, et au Fils, et au Saint‐Esprit\n**R/**Pour les siècles des siècles.\nAmen.\n",
      "titre": "Acte de contrition",
      "editeur": None,
      "auteur": None,
      "reference": "Compendium",
      "disambiguation": None,
      "langue": "francais"
    },
    {
      "id_deroule": "14",
      "cle_element": "antienne\\_3",
      "titre_particulier": "Antienne",
      "ajouter_doxologie": None,
      "element_defaut": "aelf",
      "titre": None,
      "texte": "Tout vient de lui, tout est pour lui, tout est en lui ! Gloire à Dieu dans les siècles !\n",
      "editeur": None,
      "auteur": None,
      "reference": None
    },
    {
      "id_deroule": "15",
      "cle_element": "pericope",
      "titre_particulier": "Parole de Dieu",
      "ajouter_doxologie": None,
      "element_defaut": "aelf",
      "titre": None,
      "texte": "Ce que nous proclamons, c’est, comme dit l’Écriture, ce que personne n’avait vu de ses yeux ni entendu de ses oreilles, ce que le cœur de l’homme n’avait pas imaginé, ce qui avait été préparé pour ceux qui aiment Dieu. Et c’est à nous que Dieu, par l’Esprit, a révélé cette sagesse.",
      "editeur": None,
      "auteur": None,
      "reference": "1 Co 2, 9‐10a"
    },
    {
      "id_deroule": "16",
      "cle_element": "repons",
      "titre_particulier": "Répons",
      "ajouter_doxologie": None,
      "element_defaut": "aelf",
      "titre": None,
      "texte": "**R/** Toi, le jour sans crépuscule,\nEsprit de Dieu pour notre terre,\nComment es‐tu\nLa nuit la plus obscure ?\n",
      "editeur": None,
      "auteur": None,
      "reference": None
    },
    {
      "id_deroule": "17",
      "cle_element": "antienne\\_magnificat",
      "titre_particulier": "Antienne",
      "ajouter_doxologie": None,
      "element_defaut": "aelf",
      "titre": None,
      "texte": "Un feu brûle en moi : l’amour qui consumait les prophètes et les amis du Christ, alléluia.\n",
      "editeur": None,
      "auteur": None,
      "reference": None
    },
    {
      "id_deroule": "18",
      "cle_element": "cantique\\_mariale",
      "titre_particulier": None,
      "ajouter_doxologie": "True",
      "element_defaut": "aelf",
      "titre": "Cantique de Marie",
      "texte": "**47 ** Mon âme ex__a__lte le Seigneur,\nexulte mon esprit en Die__u__, mon Sauveur !\n\n**48 ** Il s’est penché sur son h__u__mble servante ;\ndésormais, tous les âges me dir__o__nt bienheureuse.\n\n**49 ** Le Puissant fit pour m__o__i des merveilles ;\nS__a__int est son nom !\n\n**50 ** Son amour s’ét__e__nd d’âge en âge\nsur ce__u__x qui le craignent ;\n\n**51 ** Déployant la f__o__rce de son bras,\nil disp__e__rse les superbes.\n\n**52 ** Il renverse les puiss__a__nts de leurs trônes,\nil él__è__ve les humbles.\n\n**53 ** Il comble de bi__e__ns les affamés,\nrenvoie les r__i__ches les mains vides.\n\n**54 ** Il relève Isra__ë__l, son serviteur,\nil se souvi__e__nt de son amour,\n\n**55 ** de la promesse f__a__ite à nos pères,\nen faveur d’Abraham et de sa r__a__ce, à jamais.\n",
      "editeur": None,
      "auteur": None,
      "reference": "Lc 1"
    },
    {
      "id_deroule": "19",
      "cle_element": "doxologie",
      "texte": "**V/**Gloire au Père, et au Fils, et au Saint‐Esprit\n**R/**Pour les siècles des siècles.\nAmen.\n",
      "titre": "Acte de contrition",
      "editeur": None,
      "auteur": None,
      "reference": "Compendium",
      "disambiguation": None,
      "langue": "francais"
    },
    {
      "id_deroule": "20",
      "cle_element": "antienne\\_magnificat",
      "titre_particulier": "Antienne",
      "ajouter_doxologie": None,
      "element_defaut": "aelf",
      "titre": None,
      "texte": "Un feu brûle en moi : l’amour qui consumait les prophètes et les amis du Christ, alléluia.\n",
      "editeur": None,
      "auteur": None,
      "reference": None
    },
    {
      "id_deroule": "21",
      "cle_element": "intercession",
      "titre_particulier": "Intercession",
      "ajouter_doxologie": None,
      "element_defaut": "aelf",
      "titre": None,
      "texte": "Avec ceux qui ont reçu les premiers dons de l’Esprit, prions Dieu d’achever notre sanctification :\n\n**R/** Dieu notre Père, exauce‐nous\n\nDieu tout‐puissant, qui as élevé le Christ auprès de toi,\n— donne à chacun de reconnaître sa présence dans l’Église.\n\nPère, dont le Fils unique est notre chemin,\n— accorde‐nous de le suivre par‐delà la mort.\n\nEnvoie ton Esprit Saint dans le cœur des croyants,\n— pour qu’il vienne irriguer leur désert.\n\nPar la puissance de l’Esprit, conduis le cours des temps,\n— pour que la face de la terre en soit renouvelée.\n\nPère qui nous aimes sans mesure,\n— achève en toi la communion des saints.\n",
      "editeur": None,
      "auteur": None,
      "reference": None
    },
    {
      "id_deroule": "22",
      "cle_element": "oraison",
      "titre_particulier": "Oraison",
      "ajouter_doxologie": None,
      "element_defaut": "aelf",
      "titre": None,
      "texte": "Dieu qui as donné à saint Justin, ton martyr, de trouver dans la folie de la croix la connaissance incomparable de Jésus Christ, accorde‐nous, par son intercession, de rejeter les erreurs qui nous entourent et d’être affermis dans la foi.\n",
      "editeur": None,
      "auteur": None,
      "reference": None
    }
  ]
}

CONTENT_OFFICE_HTML = {'informations': {'source': 'aelf', 'zone': 'romain', 'calendrier': 'france', 'date': '2022-06-01', 'nom': 'vepres', 'couleur': 'rouge', 'annee': None, 'temps_liturgique': 'pascal', 'semaine': '7ème Semaine du Temps Pascal', 'jour': 'mercredi', 'jour_liturgique_nom': 'de la férie', 'fete': 'S. Justin, martyr', 'degre': None, 'ligne1': 'mercredi, 7ème Semaine du Temps Pascal', 'ligne2': 'S. Justin, martyr', 'ligne3': 'Mémoire', 'couleur2': None, 'couleur3': None}, 'vepres': [{'id_deroule': '0', 'cle_element': 'se_signer_office', 'titre_particulier': None, 'ajouter_doxologie': None, 'element_defaut': '1', 'titre': None, 'texte': '<i>(Faire le signe de Croix, au nom du Père, du Fils et du Saint Esprit, pendant <b>Dieu, viens à mon aide</b>)</i>', 'editeur': None, 'auteur': None, 'reference': None}, {'id_deroule': '1', 'cle_element': 'introduction', 'titre_particulier': 'Introduction', 'ajouter_doxologie': None, 'element_defaut': 'aelf', 'titre': None, 'texte': '<p><b>V/</b> Dieu, viens à mon aide,<br/><b>R/</b> Seigneur, à notre secours.</p> <p>Gloire au Père, et au Fils et au Saint-Esprit,<br/>au Dieu qui est, qui était et qui vient,<br/>pour les siècles des siècles.<br/>Amen. (Alléluia.)</p>', 'editeur': None, 'auteur': None, 'reference': None}, {'id_deroule': '2', 'cle_element': 'hymne', 'titre_particulier': 'Hymne', 'ajouter_doxologie': None, 'element_defaut': 'aelf', 'titre': 'Esprit de Dieu, tu es le feu', 'texte': "Esprit de Dieu, tu es le feu,<br/>Patiente braise dans la cendre,<br/>A tout moment prête à surprendre<br/>Le moindre souffle et à sauter<br/>Comme un éclair vif et joyeux<br/>Pour consumer en nous la paille,<br/>Eprouver l'or aux grandes flammes<br/>Du brasier de ta charité.<br/><br/>Esprit de Dieu, tu es le vent,<br/>Où prends-tu souffle, à quel rivage?<br/>Élie se cache le visage<br/>A ton silence frémissant<br/>Aux temps nouveaux tu es donné,<br/>Soupir du monde en espérance,<br/>Partout présent comme une danse,<br/>Eclosion de ta liberté.<br/><br/>Esprit de Dieu, tu es rosée<br/>De joie, de force et de tendresse,<br/>Tu es la pluie de la promesse<br/>Sur une terre abandonnée.<br/>Jaillie du Fils ressuscité,<br/>Tu nous animes, source claire,<br/>Et nous ramènes vers le Père,<br/>Au rocher de la vérité.<br/>", 'editeur': 'CNPL', 'auteur': 'CFC', 'reference': None}, {'id_deroule': '3', 'cle_element': 'antienne_1', 'titre_particulier': 'Antienne', 'ajouter_doxologie': None, 'element_defaut': 'aelf', 'titre': None, 'texte': '<p>Votre tristesse deviendra joie. Alléluia.</p>', 'editeur': None, 'auteur': None, 'reference': None}, {'id_deroule': '4', 'cle_element': 'psaume_1', 'titre_particulier': 'Psaume', 'ajouter_doxologie': 'True', 'element_defaut': 'aelf', 'titre': None, 'texte': '<p><b>1 </b> Quand le Seigneur ramena les capt<u>i</u>fs à Sion,<b>&ast;</b><br/>nous éti<u>o</u>ns comme en rêve !</p><p><b>2 </b> Alors notre bouche était pl<u>e</u>ine de rires,<br/>nous poussi<u>o</u>ns des cris de joie ; <b>&plus;</b><br/>alors on disait parm<u>i</u> les nations :<br/>« Quelles merveilles fait pour e<u>u</u>x le Seigneur ! » <b>&ast;</b><br/><b>3 </b> Quelles merveilles le Seigne<u>u</u>r fit pour nous :<br/>nous éti<u>o</u>ns en grande fête !<br/><br/><b>4 </b> Ramène, Seigne<u>u</u>r, nos captifs,<br/>comme les torr<u>e</u>nts au désert.<br/><br/><b>5 </b> Qui s<u>è</u>me dans les larmes<br/>moiss<u>o</u>nne dans la joie : <b>&plus;</b><br/><b>6 </b> il s’en va, il s’en v<u>a</u> en pleurant,<br/>il j<u>e</u>tte la semence ; <b>&ast;</b><br/>il s’en vient, il s’en vi<u>e</u>nt dans la joie,<br/>il rapp<u>o</u>rte les gerbes.</p>', 'editeur': None, 'auteur': None, 'reference': '125'}, {'id_deroule': '5', 'cle_element': 'doxologie', 'texte': '<p><b>V/</b>Gloire au Père, et au Fils, et au Saint-Esprit<br/><b>R/</b>Pour les siècles des siècles.<br/>Amen.</p>', 'titre': 'Acte de contrition', 'editeur': None, 'auteur': None, 'reference': 'Compendium', 'disambiguation': None, 'langue': 'francais'}, {'id_deroule': '6', 'cle_element': 'antienne_1', 'titre_particulier': 'Antienne', 'ajouter_doxologie': None, 'element_defaut': 'aelf', 'titre': None, 'texte': '<p>Votre tristesse deviendra joie. Alléluia.</p>', 'editeur': None, 'auteur': None, 'reference': None}, {'id_deroule': '7', 'cle_element': 'antienne_2', 'titre_particulier': 'Antienne', 'ajouter_doxologie': None, 'element_defaut': 'aelf', 'titre': None, 'texte': '<p>Que nous vivions, que nous mourions, c’est pour le Seigneur, alléluia.</p>', 'editeur': None, 'auteur': None, 'reference': None}, {'id_deroule': '8', 'cle_element': 'psaume_2', 'titre_particulier': 'Psaume', 'ajouter_doxologie': 'True', 'element_defaut': 'aelf', 'titre': None, 'texte': '<b>1 </b> Si le Seigneur ne bât<u>i</u>t la maison,<br/>les bâtisseurs trav<u>a</u>illent en vain ; <b>&ast;</b><br/>si le Seigneur ne g<u>a</u>rde la ville,<br/>c’est en vain que v<u>e</u>illent les gardes.<br/><br/><b>2 </b> En vain tu dev<u>a</u>nces le jour,<br/>tu retardes le mom<u>e</u>nt de ton repos, <b>&plus;</b><br/>tu manges un p<u>a</u>in de douleur : <b>&ast;</b><br/>Dieu comble son bien-aim<u>é</u> quand il dort.<br/><br/><b>3 </b> Des fils, voilà ce que d<u>o</u>nne le Seigneur,<br/>des enfants, la récomp<u>e</u>nse qu’il accorde ; <b>&ast;</b><br/><b>4 </b> comme des flèches aux m<u>a</u>ins d’un guerrier,<br/>ainsi les f<u>i</u>ls de la jeunesse.<br/><br/><b>5 </b> Heureux l’h<u>o</u>mme vaillant<br/>qui a garni son carqu<u>o</u>is de telles armes ! <b>&ast;</b><br/>S’ils affrontent leurs ennem<u>i</u>s sur la place,<br/>ils ne seront p<u>a</u>s humiliés.<br/>', 'editeur': None, 'auteur': None, 'reference': '126'}, {'id_deroule': '9', 'cle_element': 'doxologie', 'texte': '<p><b>V/</b>Gloire au Père, et au Fils, et au Saint-Esprit<br/><b>R/</b>Pour les siècles des siècles.<br/>Amen.</p>', 'titre': 'Acte de contrition', 'editeur': None, 'auteur': None, 'reference': 'Compendium', 'disambiguation': None, 'langue': 'francais'}, {'id_deroule': '10', 'cle_element': 'antienne_2', 'titre_particulier': 'Antienne', 'ajouter_doxologie': None, 'element_defaut': 'aelf', 'titre': None, 'texte': '<p>Que nous vivions, que nous mourions, c’est pour le Seigneur, alléluia.</p>', 'editeur': None, 'auteur': None, 'reference': None}, {'id_deroule': '11', 'cle_element': 'antienne_3', 'titre_particulier': 'Antienne', 'ajouter_doxologie': None, 'element_defaut': 'aelf', 'titre': None, 'texte': '<p>Tout vient de lui, tout est pour lui, tout est en lui ! Gloire à Dieu dans les siècles !</p>', 'editeur': None, 'auteur': None, 'reference': None}, {'id_deroule': '12', 'cle_element': 'psaume_3', 'titre_particulier': 'Psaume', 'ajouter_doxologie': 'True', 'element_defaut': 'aelf', 'titre': None, 'texte': "<p><b>12 </b> Rendons grâce à Die<u>u</u> le Père, <b>&plus;</b><br/>lui qui nous a donné<br/>d’avoir part à l’hérit<u>a</u>ge des saints, <b>&ast;</b><br/>d<u>a</u>ns la lumière.<br/><br/><b>13 </b> Nous arrachant à la puiss<u>a</u>nce des ténèbres, <b>&plus;</b><br/>il nous a placés<br/>dans le Royaume de son F<u>i</u>ls bien-aimé : <b>&ast;</b><br/><b>14 </b> en lui nous avons le rachat,<br/>le pard<u>o</u>n des péchés.<br/><br/><b>15 </b> Il est l’image du Die<u>u</u> invisible, <b>&plus;</b><br/>le premier-né, avant to<u>u</u>te créature : <b>&ast;</b><br/><b>16 </b> en lui, tout fut créé,<br/>dans le ci<u>e</u>l et sur la terre.<br/><br/>Les êtres vis<u>i</u>bles et invisibles, <b>&plus;</b><br/>puissances, principautés,<br/>souverainet<u>é</u>s, dominations, <b>&ast;</b><br/>tout est créé par lu<u>i</u> et pour lui.<br/><br/><b>17 </b> Il est avant to<u>u</u>te chose,<br/>et tout subs<u>i</u>ste en lui.<br/><br/><b>18 </b> Il est aussi la t<u>ê</u>te du corps, la tête de l’Église : <b>&plus;</b><br/>c'est lui le commencement,<br/>le premier-n<u>é</u> d’entre les morts, <b>&ast;</b><br/>afin qu'il ait en to<u>u</u>t la primauté.<br/><br/><b>19 </b> Car Dieu a jugé bon<br/>qu'habite en lui to<u>u</u>te plénitude <b>&ast;</b><br/><b>20 </b> et que tout, par le Christ,<br/>lui soit enf<u>i</u>n réconcilié,<br/><br/>faisant la paix par le s<u>a</u>ng de sa Croix, <b>&ast;</b><br/>la paix pour tous les êtres<br/>sur la t<u>e</u>rre et dans le ciel.</p>", 'editeur': None, 'auteur': None, 'reference': 'CANTIQUE (Col 1)'}, {'id_deroule': '13', 'cle_element': 'doxologie', 'texte': '<p><b>V/</b>Gloire au Père, et au Fils, et au Saint-Esprit<br/><b>R/</b>Pour les siècles des siècles.<br/>Amen.</p>', 'titre': 'Acte de contrition', 'editeur': None, 'auteur': None, 'reference': 'Compendium', 'disambiguation': None, 'langue': 'francais'}, {'id_deroule': '14', 'cle_element': 'antienne_3', 'titre_particulier': 'Antienne', 'ajouter_doxologie': None, 'element_defaut': 'aelf', 'titre': None, 'texte': '<p>Tout vient de lui, tout est pour lui, tout est en lui ! Gloire à Dieu dans les siècles !</p>', 'editeur': None, 'auteur': None, 'reference': None}, {'id_deroule': '15', 'cle_element': 'pericope', 'titre_particulier': 'Parole de Dieu', 'ajouter_doxologie': None, 'element_defaut': 'aelf', 'titre': None, 'texte': 'Ce que nous proclamons, c’est, comme dit l’Écriture, ce que personne n’avait vu de ses yeux ni entendu de ses oreilles, ce que le cœur de l’homme n’avait pas imaginé, ce qui avait été préparé pour ceux qui aiment Dieu. Et c’est à nous que Dieu, par l’Esprit, a révélé cette sagesse.', 'editeur': None, 'auteur': None, 'reference': '1 Co 2, 9-10a'}, {'id_deroule': '16', 'cle_element': 'repons', 'titre_particulier': 'Répons', 'ajouter_doxologie': None, 'element_defaut': 'aelf', 'titre': None, 'texte': '<p><b>R/</b> Toi, le jour sans crépuscule,<br/>Esprit de Dieu pour notre terre,<br/>Comment es-tu<br/>La nuit la plus obscure ?</p>', 'editeur': None, 'auteur': None, 'reference': None}, {'id_deroule': '17', 'cle_element': 'antienne_magnificat', 'titre_particulier': 'Antienne', 'ajouter_doxologie': None, 'element_defaut': 'aelf', 'titre': None, 'texte': "<p>Un feu brûle en moi : l’amour qui consumait les prophètes et les amis du Christ, alléluia.</p>", 'editeur': None, 'auteur': None, 'reference': None}, {'id_deroule': '18', 'cle_element': 'cantique_mariale', 'titre_particulier': None, 'ajouter_doxologie': 'True', 'element_defaut': 'aelf', 'titre': 'Cantique de Marie', 'texte': "<p><b>47 </b> Mon âme ex<u>a</u>lte le Seigneur,<br/>exulte mon esprit en Die<u>u</u>, mon Sauveur !<br/><br/><b>48 </b> Il s’est penché sur son h<u>u</u>mble servante ;<br/>désormais, tous les âges me dir<u>o</u>nt bienheureuse.<br/><br/><b>49 </b> Le Puissant fit pour m<u>o</u>i des merveilles ;<br/>S<u>a</u>int est son nom !<br/><br/><b>50 </b> Son amour s’ét<u>e</u>nd d’âge en âge<br/>sur ce<u>u</u>x qui le craignent ;<br/><br/><b>51 </b> Déployant la f<u>o</u>rce de son bras,<br/>il disp<u>e</u>rse les superbes.<br/><br/><b>52 </b> Il renverse les puiss<u>a</u>nts de leurs trônes,<br/>il él<u>è</u>ve les humbles.<br/><br/><b>53 </b> Il comble de bi<u>e</u>ns les affamés,<br/>renvoie les r<u>i</u>ches les mains vides.<br/><br/><b>54 </b> Il relève Isra<u>ë</u>l, son serviteur,<br/>il se souvi<u>e</u>nt de son amour,<br/><br/><b>55 </b> de la promesse f<u>a</u>ite à nos pères,<br/>en faveur d’Abraham et de sa r<u>a</u>ce, à jamais.</p>", 'editeur': None, 'auteur': None, 'reference': 'Lc 1'}, {'id_deroule': '19', 'cle_element': 'doxologie', 'texte': '<p><b>V/</b>Gloire au Père, et au Fils, et au Saint-Esprit<br/><b>R/</b>Pour les siècles des siècles.<br/>Amen.</p>', 'titre': 'Acte de contrition', 'editeur': None, 'auteur': None, 'reference': 'Compendium', 'disambiguation': None, 'langue': 'francais'}, {'id_deroule': '20', 'cle_element': 'antienne_magnificat', 'titre_particulier': 'Antienne', 'ajouter_doxologie': None, 'element_defaut': 'aelf', 'titre': None, 'texte': "<p>Un feu brûle en moi : l’amour qui consumait les prophètes et les amis du Christ, alléluia.</p>", 'editeur': None, 'auteur': None, 'reference': None}, {'id_deroule': '21', 'cle_element': 'intercession', 'titre_particulier': 'Intercession', 'ajouter_doxologie': None, 'element_defaut': 'aelf', 'titre': None, 'texte': '<p>Avec ceux qui ont reçu les premiers dons de l’Esprit, prions Dieu d’achever notre sanctification :</p><br/><b>R/</b> <p>Dieu notre Père, exauce-nous</p><br/><p>Dieu tout-puissant, qui as élevé le Christ auprès de toi,<br/>— donne à chacun de reconnaître sa présence dans l’Église.</p><br/><p>Père, dont le Fils unique est notre chemin,<br/>— accorde-nous de le suivre par-delà la mort.</p><br/><p>Envoie ton Esprit Saint dans le cœur des croyants,<br/>— pour qu’il vienne irriguer leur désert.</p><br/><p>Par la puissance de l’Esprit, conduis le cours des temps,<br/>— pour que la face de la terre en soit renouvelée.</p><br/><p>Père qui nous aimes sans mesure,<br/>— achève en toi la communion des saints.</p>', 'editeur': None, 'auteur': None, 'reference': None}, {'id_deroule': '22', 'cle_element': 'oraison', 'titre_particulier': 'Oraison', 'ajouter_doxologie': None, 'element_defaut': 'aelf', 'titre': None, 'texte': "<p>Dieu qui as donné à saint Justin, ton martyr, de trouver dans la folie de la croix la connaissance incomparable de Jésus Christ, accorde-nous, par son intercession, de rejeter les erreurs qui nous entourent et d’être affermis dans la foi.</p>", 'editeur': None, 'auteur': None, 'reference': None}]}


CONTENT_OFFICE_SIMPLE_HTML = {}
