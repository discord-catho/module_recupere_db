# -*- coding: utf-8 -*-
import unittest  # python -m unittest discover
import sys
import os
import subprocess
import io
import json
from dktotoolkit import load_dotenv
from _office_datas import CONTENT_OFFICE_MARKDOWN, CONTENT_OFFICE_HTML

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#

sys.path.insert(0, '../')
from dktorecuperedb import getOffice


class Test_Office(unittest.TestCase):
    def setUp(self):
        # Code execute avant chaque test

        self.cal  = "france"
        self.office = "vepres"
        self.format_output="html" # "simple_html" / "markdown"
        self.date = "2022-06-01"

        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(str2digit)
        #
    #endDef

    def testOffice_date(self):
        from datetime import date
        today = str(date.today())

        load_dotenv()

        result = getOffice(
            calendar=self.cal,
            office=self.office,
            date="today",
            format_output="html")["informations"]["date"]

        self.assertEqual(result, today)
    #endDef

    def testOffice_html(self):

        load_dotenv()
        #self.maxDiff = None
        result = getOffice(
            calendar=self.cal,
            office=self.office,
            date=self.date,
            format_output="html")

        self.assertEqual(result, CONTENT_OFFICE_HTML)
    #endDef

    def testOffice_markdown(self):

        load_dotenv()

        result = getOffice(
            calendar=self.cal,
            office=self.office,
            date=self.date,
            format_output="markdown")
        #self.maxDiff  = None
        with open("datas/test_office_markdown.dat", "w", encoding="utf-8") as f:
            # Écrire le dictionnaire dans le fichier JSON en désactivant l'encodage ASCII
            json.dump(result, f, indent=2, ensure_ascii=False)
        #
        self.maxDiff = None
        self.assertEqual(result, CONTENT_OFFICE_MARKDOWN)
    #endDef

    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
           output_folder="./profile"
           proffile_basename = hr.names2basename(
               file_name = __file__,
               class_name = self.__class__.__name__,
               method_name = self._testMethodName
           )

           hr.tearDown_profiler(
               profiler=self.profiler,
               proffile_basename=proffile_basename,
               output_folder=output_folder,
               delete_profiling_env=self.delete_profiling_env
           )

        pass
    #endDef
#endClass
