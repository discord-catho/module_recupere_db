# -*- coding: utf-8 -*-
import unittest  # python -m unittest discover
import sys
import os
import subprocess
import io

from dktotoolkit import load_dotenv
from _compendium_datas import COMPENDIUM_CREDO

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#

sys.path.insert(0, '../')
from dktorecuperedb import getCompendium


class Test_Compendium(unittest.TestCase):
    def setUp(self):
        # Code execute avant chaque test

        self.collection  = "compendium"
        self.title = "Credo"
        self.language="francais"
        self.disambiguation = "Nicée-Constantinople"

        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(str2digit)
        #
    #endDef

    def test_colltitle(self):


        load_dotenv()

        result = getCompendium(collection=self.collection,name=self.title)
    
        self.assertEqual(result, COMPENDIUM_CREDO)
    #endDef


    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
           output_folder="./profile"
           proffile_basename = hr.names2basename(
               file_name = __file__,
               class_name = self.__class__.__name__,
               method_name = self._testMethodName
           )

           hr.tearDown_profiler(
               profiler=self.profiler,
               proffile_basename=proffile_basename,
               output_folder=output_folder,
               delete_profiling_env=self.delete_profiling_env
           )


        pass
    #endDef
#endClass

