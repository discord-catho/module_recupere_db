# -*- coding: utf-8 -*-
import unittest  # python -m unittest discover
import sys
import os
import subprocess
import io

from dktotoolkit import load_dotenv
# from _compendium_datas import COMPENDIUM_CREDO

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#

sys.path.insert(0, '../')
from dktorecuperedb import getBible

JEAN115="""<span class="chapter_number">1</span>  <span class="verse_number">1</span> AU COMMENCEMENT était le Verbe,
et le Verbe était auprès de Dieu,
et le Verbe était Dieu.
 <span class="verse_number">2</span> Il était au commencement auprès de Dieu.
 <span class="verse_number">3</span> C’est par lui que tout est venu à l’existence,
et rien de ce qui s’est fait ne s’est fait sans lui.
 <span class="verse_number">4</span> En lui était la vie,
et la vie était la lumière des hommes ;
 <span class="verse_number">5</span> la lumière brille dans les ténèbres,
et les ténèbres ne l’ont pas arrêtée"""


class Test_Bible(unittest.TestCase):
    def setUp(self):
        # Code execute avant chaque test

        self.collection  = "compendium"
        self.title = "Credo"
        self.language="francais"
        self.disambiguation = "Nicée-Constantinople"

        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(str2digit)
        #
    #endDef

    def test_onestr(self):

        load_dotenv()

        result = getBible("Jn 1:1-5")
   
        self.assertEqual(result, JEAN115)
    #endDef

    def test_twostr(self):

        load_dotenv()

        result = getBible("Jn" ,"1:1-5")
   
        self.assertEqual(result, JEAN115)
    #endDef


    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
           output_folder="./profile"
           proffile_basename = hr.names2basename(
               file_name = __file__,
               class_name = self.__class__.__name__,
               method_name = self._testMethodName
           )

           hr.tearDown_profiler(
               profiler=self.profiler,
               proffile_basename=proffile_basename,
               output_folder=output_folder,
               delete_profiling_env=self.delete_profiling_env
           )


        pass
    #endDef
#endClass

