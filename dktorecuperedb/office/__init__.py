import os
import sys
import json

from dktotoolkit import parser_date, compat_mode, write_message
#from . import call_api(source=aelf, ...)
#from . import parser(dico/list/str, output_format)

def _todict(obj):
    # https://github.com/matthewwithanm/python-markdownify/blob/develop/markdownify/__init__.py
    return dict((k, getattr(obj, k)) for k in dir(obj) if not k.startswith('_'))
#

"""
Database:
---------

* All datas are in HTML - ascii

[office] :
- id_office => index
- source       = source de l'office = aelf uniquement pour le moement
- date_requete = yyyy-mm-dd
- zone         = calendrier  (AELF)
- calendrier   = calendrier  (dans le cas ou on utilise la zone "romain" pour un calendrier francais par exemple)
- date         = yyyy-mm-dd  (AELF)
- nom          = nom de l'office (AELF)
- couleur      = couleur liturgique (AELF)
- annee        = A, B ou C (pour les messes uniquement) (AELF)
- temps_liturgique = (AELF)
- semaine = (AELF)
- jour = (AELF)
- jour_liturgique_nom = (AELF)
- fete = (AELF)
- degre = (AELF)
- ligne1 = (AELF)
- ligne2 = (AELF)
- ligne3 = (AELF)
- couleur2 = (AELF)
- couleur3 = (AELF)

[liaison] :
- id_office
- id_element
- cle_element

[element] :
- id_element
- titre
- texte
- editeur
- auteur
- reference
# Remarque : a la difference d'AELF, je renvoie toujours cette structure, et éléments vides avec None

[deroule]:
- id_deroule => index
- nom_office
- id_deroule : position (exemple : intro des vepres = 0, confiteor des vepres = 1, hymne des vepres = 2, ...)
- cle_element : exemple : "psaume1", "antienne1" : bref, les cles d'AELF
- titre_particulier : pour forcer le titre
- ajouter_doxologie : ajouter la doxologie apres l'element s'il n'est pas vide
- element_defaut : aelf ou nom de la variante dans la BD compendium


                +--------------+
                |   office     |
                +--------------+
                  | id_office (PK)
                  | ...
                  |
                  |     +----------------+
                  |     |   liaison      |
                  |     +----------------+
                  |     | id_office (FK) |
                  |     | id_element (FK)|
                  |     | cle_element    |
                  |     +----------------+
                  |
                  |     +-----------------+
                  +---->|    element      |
                  |     +-----------------+
                  |     | id_element (PK) |
                  |     | titre           |
                  |     | texte           |
                  |     | editeur         |
                  |     | auteur          |
                  |     | reference       |
                  |     +-----------------+
                  |
                  |     +-------------------+
                  +---->|    deroule        |
                        +-------------------+
                        | id_deroule (PK)   |
                        | nom_office        |
                        | id_deroule        |
                        | cle_element       |
                        | titre_particulier |
                        | ajouter_doxologie |
                        | element_defaut    |
                        +-------------------+

"""
import os
chemin_parent = os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), os.pardir))
chemin_externe = os.path.join(chemin_parent, '__externals__', 'aelf-prayondiscord')
#sys.path.insert(-1, chemin_externe)
sys.path.insert(-1, "/home/pierre/Documents/Projets/catho/aelf-praywithdiscord/")

from aelf_prayondiscord_as_module.utils.aelf import AELF

class Office(AELF):
    """
RECUPERER LES INFOS DU BOT (office)

:param list(str) available_source: = ["aelf", ]
:param list(str) available_format_output: = ["html", "markdown"]
:param list(str) available_calendar: = ["francais", "romain"]
:param list(str) available_office: = ["vepres", "complies"] #TODO : a remplir !!

:param str format_output: Format de sortie
:param str date: date (YYYY-MM-DD) Date de l'office
:param str calendar: Calendrier / zone utilisé
:param str office: nom de l'office
:param str source: source principale de l'office (uniquement AELF actuellement)
"""
    class DefaultOptions:
        available_source = ["aelf", ]
        available_format_output = ["native", "simple_html", "clean_html", "html", "markdown", "discord"]

        format_output="html"

        use_hunfolding=False

        details=False

        fill_datas=True
    #

    class Options(DefaultOptions):
        use_hunfolding=True
        # format_output="discord"
        pass
    #

    def __init__(self, **kwargs):
        """
Constructeur

:param str formatting: [html, markdown] format de sortie
:param str zone: zone
:param str|Date date: date de l'office
:param str office: nom de l'office
:param str source: source (AELF uniquement actuellement)
"""
        # Create an options dictionary. Use DefaultOptions as a base so that
        # it doesn't have to be extended.
        # https://github.com/matthewwithanm/python-markdownify/blob/develop/markdownify/__init__.py
        super().__init__(**kwargs)

        self.options.update(_todict(Office.DefaultOptions))
        self.options.update(_todict(Office.Options))

        self.options.update(kwargs)

        #self.format_output = self.options.get("format_output", None).lower()
        if self.options.get("date", None):
            self.date = parser_date(self.options.get("date", None).lower())  # Appeler la fonction "convert_date" ou jsp comment : YYYY-MM-DD
        else:
            self.date = None
        #
        self.zone = self.options.get("zone", None).lower()
        #self.source = self.options.get("source", None).lower()
        self.office = self.options.get("office", None).lower()

    #endDef


    # Update
    from ._update_hunfolding import update_hunfolding

    #from ._check_input import check_input

    from ._get_info_from_db import get_info_from_db
    from ._get_office import get_office_hunfolding
    from ._get_hunfolding import get_hunfolding
    from ._get_alternatives import get_alternatives

    from ._hunfolding_from_json import hunfolding_from_json

    from ._fill_datas_calling_db import fill_datas_calling_db
    from ._parse_datas import parse_datas

    def update_attributes(self, format_output:str=None,**kwargs):
        """
        Update attributes of the class from the kwargs
        """
        if format_output is None:
            pass
        elif not isinstance(format_output, str):
            sys.stderr.write(f"> Office.get_office(): format_output={format_output} must be a string\n")
        elif format_output.lower() in self.available_format_output:
            self.format_output = format_output.lower()
        elif format_output.lower() not in self.available_format_output:
            sys.stderr.write(f"> Office.get_office(): format_output={format_output} not available ; please use one in {self.available_format_output}\n")
        else:
            sys.stderr.write(f"> Office.get_office(): format_output={format_output} unexpected\n")
        #endIf

        return kwargs
    #endDef

    def update_db(self):
        self.update_hunfolding()
    #endDef

    # Surcharge : obligation de les ecrire ici a cause de super()
    def check_input(self):

        """
        Vérifie les paramètres d'entrée de l'objet OfficeAELF.

        :returns: Un dictionnaire contenant le statut de vérification.
        - Si les paramètres sont valides, le dictionnaire contient {"status": "200"}.
        - Si des erreurs sont détectées, le dictionnaire contient {"status": "404", "error": "<liste_des_erreurs>"}.
        :rtype: dict
        """

        d = AELF(**self.options).check_input()

        err =  d.get("error", [])

        if self.options["source"] is None:
            write_message("source is not setted", level="warning")
        elif self.options["source"].lower() not in self.options["available_source"]:
            msg=f"OfficeAELF.__init__() : source = {self.options['source']}, not in {self.options['available_source']} ! I'll use '{self.options['available_source'][0]}'"
            write_message(msg, level="warning")
            err += [msg,]
            self.options["source"] = self.options['available_source'][0]
        #endIf

        if err:
            sys.stderr.write(">>"+"\n>>".join(err))
            return {"status" : 404, "error" : "\n".join(err)}
        else:
            return {"status":200}
        #endIf

        raise Exception
    #endDef

#endClass
