import sys
import json
import re
import bs4

if __name__=="__main__":
    import os
    sys.path.insert(0, os.path.abspath('../..'))
#end

from dktotoolkit.dict import unprefix_keys

from .db import OfficeDB



SKIP_PARSER_INFOS=["date", "date_requete", "id_office"]
SKIP_PARSER_ELTS=["cle_element", "element_defaut", "reference", "id_office", "nom_office"]


def _split_kwargs(**kwargs):
    format_kwargs = unprefix_keys(dico=kwargs, prefix="format_")
    office_kwargs = unprefix_keys(dico=kwargs, prefix="office_")
    kwargs = {k:kwargs[k] for k in set(kwargs.keys())-set(["format_"+i for i in format_kwargs.keys()]) - set(["office_"+i for i in format_kwargs.keys()])}

    return format_kwargs, office_kwargs, kwargs

def get_office_hunfolding(self, **kwargs)->None:
    """
    Recuperer tout l'office a partir de la base de donnee, retourne les donnees au format self.format_output

    :param str kwargs[format_X]: output format (format_discordwrap, format_discordwrap_width)
    :param str kwargs[office_Y]: office kwargs (office_doxologie, ...)
    :returns: office
    :rtypes: dict
    """

    status = self.check_input()
    if status.get("status", -1) not in [200, 202]:

        self.data.update(status)

        return status.get("status", -1)
    #

    format_kwargs, office_kwargs, kwargs = _split_kwargs(**kwargs)

    # 3. (dict) info, (dict) hunfolding, (dict) office (=nom_office) = call_db_complet(date_office, calendrier, source="aelf"))

    hunfolding = []

    if self.options.get("use_hunfolding", False):  # https://localhost:8000/v1/Sequence/{self.office}
        with OfficeDB() as db:
            hunfolding = db.get_hunfolding(self.zone, self.date, self.office, source=self.options["source"], details=self.options["details"])
        #
    #

    if not self.options["source"]:

        self.datas[self.office] = hunfolding

    elif self.options["source"] == "aelf":

        self.get_office(hunfolding=hunfolding, format_output="html") # maj self.datas (meme si le retourne aussi)

    else:
        raise ValueError("source must be AELF or None")
    #

    # 4. Si formatting = html : conversion recursive
    # 4. Si formatting = markdown : conversion recursive

    if self.options.get("fill_datas"):
        self.fill_datas_calling_db(**office_kwargs)
    #

    self.parse_datas(**format_kwargs)

    self.datas.update(status)

    return self.datas
#endDef
