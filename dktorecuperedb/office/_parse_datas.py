from dktotoolkit import ParserHTML

def parse_datas(
        self,
        simplify_tags:dict = {"span:class_=verse_number":"i", "span:class_=chapter_number":"b"},
        **kwargs
) -> None:

    """
    Analyse les données en fonction du format de sortie spécifié.

    :param self: L'instance de la classe.
    :param dict simplify_tags: Balises à simplifier dans le HTML (par défaut, {"span:class_=verse_number":"i", "span:class_=chapter_number":"b"}).
    :param kwargs: Arguments de mots-clés optionnels.

    :return: None

    :raise ValueError: Si `format_output` n'est pas une chaîne de caractères ou est incorrect.
    :raise: Propage les erreurs de la classe Compendium.

    Cette fonction prend en charge différentes opérations selon le format de sortie spécifié (`format_output`).

    - Si `format_output` est None ou est l'une des valeurs "html" ou "native", aucun traitement n'est effectué.
    - Si `format_output` est une chaîne de caractères autre que celles précédemment mentionnées, une erreur de valeur est levée.

    La fonction utilise un dictionnaire `simplify_tags` pour spécifier quelles balises HTML doivent être simplifiées.

    Si `format_output` est "clean_html" ou "simple_html", la fonction nettoie et simplifie le HTML en utilisant `simplify_tags`.

    Si `format_output` est "markdown", la fonction convertit le HTML en format Markdown.

    Si `format_output` est "discord", la fonction adapte le texte pour un affichage correct sur Discord.
    """

    format_output = self.options.get('format_output', '').lower()

    if kwargs.get('format_output'):
        format_output = kwargs.get('format_output').lower()
    #

    if format_output is None or format_output.lower() in ["html", "native"]:

        return

    elif not isinstance(format_output, str):

        raise ValueError(f"format_output must be a string {self.format_output}")

    #
    info_parsed = ParserHTML(
            self.datas.get("informations",{}),
            convertEmptyNone=True,
            convert_keys=False,
        )

    elements_parsed=ParserHTML(
            self.datas.get(self.office,[]),
            convertEmptyNone=True,
            convert_keys=False,
        )

    if format_output.lower() in ["clean_html", "simple_html"]:

        kwargs = {"inplace":False, "replace_tags":simplify_tags}

        self.datas["informations"] = info_parsed.clean_html(**kwargs)
        self.datas[self.office] = elements_parsed.clean_html(**kwargs)

    elif format_output.lower()=="markdown":

        kwargs.update({"inplace":False, "replace_tags":simplify_tags})

        self.datas["informations"] = info_parsed.to_markdown(**kwargs)
        self.datas[self.office] = elements_parsed.to_markdown(**kwargs)

    elif format_output.lower()=="discord":

        kwargs["discordwrap"]=kwargs.get(
            "discordwrap", True
        )

        kwargs["discordwrap_width"]=kwargs.get(
            "discordwrap_width", 1020
        )
        kwargs["discordwrap_keeplines"]=kwargs.get(
            "discordwrap_keeplines", False
        )
        kwargs.update({"discord_key":"texte"})
        kwargs.update({"replace_tags":simplify_tags})

        kwargs.update({"inplace":False, "discord":True})

        self.datas["informations"] = info_parsed.to_markdown(**kwargs)
        self.datas[self.office] = elements_parsed.to_markdown(**kwargs)

    else:

        raise ValueError(f"format_output not expected {self.format_output}")
    #


