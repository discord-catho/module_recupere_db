from .db import OfficeDB

from dktotoolkit import ParserHTML

from ._get_office import _split_kwargs

def get_hunfolding(self, inplace=False, **kwargs):
    """Recuperer uniquement le deroulement, retourne les donnees au format self.format_output (a priori, pas d'importance, mais bon)
TODO : sortir les références du jour !!!

    :returns:  [{"position_element_deroule", "cle_element"}]
    :rtypes: list(dict(str:str))

    Voir aussi
    ----------
    :func:`OfficeDB.get_hunfolding`
    """


    self.options["fill_datas"] = True
    self.source = None
    self.options["use_hunfolding"] = True
    return self.get_office(**kwargs)


#endDef
