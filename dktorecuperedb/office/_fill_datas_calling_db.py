from dktotoolkit import write_message

from ..compendium import Compendium

KEY_NAME="key_name" # ex :cle_element

def fill_datas_calling_db(self, **kwargs):
    """
    Remplit les données en appelant la base de données.

    Cette fonction parcourt les éléments associés à `self.office` et effectue différentes opérations sur les données.

    1. Extraction des éléments associés à `self.office`.
    2. Ajout des éléments avec la clé "default_element_key" à `kwargs`.
    3. Modification de `kwargs` en ajoutant la clé "doxologie" si elle n'est pas présente.
       Sinon, réinitialisation de `kwargs` avec "doxologie_court".
    4. Parcours des éléments de `kwargs` et traitement des données correspondantes.
    5. Mise à jour de `self.datas[self.office]` avec les éléments modifiés.

    :param self: L'instance de la classe.
    :param kwargs: Arguments de mots-clés attendus.

    :return: None

    :raise: progapate errors from Compendium
    :raise: Get datas from compendium has not status 200
    """

    elements = self.datas.get(self.office,[])

    # Ajout des donnees par default:
    for element in elements:
        if element.get("default_element_key", False):
            kwargs[element.get(KEY_NAME, None)] = element.get("default_element_key", None)
        #
    #

    # Modification des donnees (office_kwargs):
    if kwargs and not "doxologie" in kwargs.keys():
        kwargs["doxologie"] = "doxologie_court"
    else:
        kwargs = {"doxologie":"doxologie_court"}
    #endIf

    for k, v in kwargs.items():

        cles = [e[KEY_NAME] if KEY_NAME in e.keys() else None for e in elements]
        mask_cle = [k == e for e in cles]

        if v is None or not v or v.lower() == "aelf": # utiliser la valeur par defaut
            continue
        #endIf

        if not k in cles: # Ne pas ajouter d'element qui ne serait pas présent dans le déroulé
            write_message(f"Key {k} is not in the hunfolding: {cles}\n", level="warning")
            continue
        #endIf

        comp = {}

        try:

            comp = Compendium(key=v)   # https://localhost:8000/v1/Compendium/{v}
            content = comp.get_content()

        except Exception as e:

            write_message(f"dktorecuperedb.Office.get_office : Exception (1) : {e}", level="critical")
            raise

        #

        if content.get("status", -1) == 202:

            content_renamed={
                "titre": content[v]["name"],
                "texte": content[v]["content"],
                "editeur": content[v]["editor"],
                "auteur": content[v]["author"],
                "reference": content[v]["collection"],
                "disambiguation":content[v]["disambiguation"],
                "langue":content[v]["language"],
            }
            content = content_renamed

            for i in range(len(elements)):
                if mask_cle[i] and isinstance(content, dict):
                    for k, v in content.items():
                        elements[i][k] = v
                    #endFor
                #endIf
            #endFor
        else:
            write_message(f"Error {content.get('status',-999)} while datas is getted", level='critical')
            raise
        #endIf

    #endFor

    self.datas[self.office] = elements
