__title__ = "recupere_db"
__pkg_name__ = "dktorecuperedb"
__version__ = "0.1.0b8"
__license__ = "AGPL 3"
__copyright__ = "2023, Pierre"

__git_name__ = "recupere_db"
__git_group__ = "1sixunhuit"

__author__ = "Pierre"
__author_email__ = "pierre@exemple.com"

__description__ = "RECUPERE databases"
__keywords__ = ["profiling", "profiler", "line_profiler", "html", "report"]

__python_requires__=">=3.9, <4"
# requirements are in requirements.txt
