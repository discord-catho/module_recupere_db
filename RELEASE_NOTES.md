# Release Notes


## Version 0.1.0b8
22/09/2023

### New Features
- "discord" format for office by default

### Improvements
- Office class (simplify, move apis sources into dktotoolkit), not out an item if not text and not default key, overload aelf-prayondiscord class (submodule)
- Compendium class manage all stuff for compendium, possible to call key or usage
- Status code
- Use new version of dktotoolkit

### Bug Fixes
- Use new version of dtkotoolkit
- Rewrite gitlab-ci (based on dktotoolkit CI)