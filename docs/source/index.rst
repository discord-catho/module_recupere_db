.. RECUPERE_db documentation master file, created by
   sphinx-quickstart on Sat May 27 15:40:22 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to RECUPERE_db's documentation!
=======================================

.. important::   R.E.C.U.P.E.R.E.

   **R**\ echerche d'**É**\ léments **C**\ atholiques **U**\ nifiés pour la **P**\ rière, l'**É**\ dification, la **R**\ éflexion et l'**E**\ nrichissement


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   office
   compendium
   bible


Package
=======
Téléchargement
-------------

Vous pouvez télécharger la dernière version du package au format wheel :

- `recupere_db-0.0.1-py3-none-any.whl <./recupere_db-0.0.1-py3-none-any.whl>`_


.. note::
   Assurez-vous d'utiliser la bonne version et l'architecture appropriée du fichier wheel en fonction de votre système d'exploitation.

.. _./recupere_db-0.0.1-py3-none-any.whl: ./recupere_db-0.0.1-py3-none-any.whl



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
