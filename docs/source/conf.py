import os
import sys
#sys.path.insert(0, os.path.abspath('../'))
sys.path.insert(0, os.path.abspath("../.."))
#os.environ["PYTHONPATH"] = "../"

# MODIFY =====================================================
import dktorecuperedb as prjt


# DO NOT MODIFY ==============================================

# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = prjt.__title__
copyright = prjt.__copyright__
author = prjt.__author__
release = prjt.__version__

version_path = os.path.abspath(os.path.join("../..", prjt.__pkg_name__, "__version__.py"))
if not os.path.exists(version_path):
    raise ValueError(f"{version_path} not exists")
#endIf

rst_prolog = f"""
.. |version_file| replace:: {version_path}
.. |pkg_name| replace:: {prjt.__pkg_name__}
"""


rst_prolog = f"""
.. |version_file| replace:: {version_path}
"""

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx_rtd_theme',
    'sphinx.ext.autodoc',  # Autodoc
    'sphinx.ext.autosummary',  # Ne pas ecrire les autoX
    'sphinx.ext.viewcode', # Arborescence
    "myst_parser",    # Add the README
]

# inutile :
#'sphinx_substitution_extensions'
# A ajouter :
#    "sphinx.ext.intersphinx",
#    "sphinx.ext.todo",
# highlight_language = ['python', 'http'] # reconnaître et mettre en évidence le code HTTP avec la directive http:highlight.
#'sphinx_automodapi.automodapi',
#'sphinxcontrib.httpdomain', #API

templates_path = ['_templates']
exclude_patterns = []

language = 'fr'

os.environ['SPHINX_BUILD'] = '1'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
# html_static_path = ['_static']

autodoc_default_options = {
    'members': True,
    'member-order': 'bysource',
    'special-members': '__init__',
    'undoc-members': True,
    'exclude-members': '__weakref__',
    'no-module-first':True
}
